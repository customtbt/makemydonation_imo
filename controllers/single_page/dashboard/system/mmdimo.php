<?php
namespace Concrete\Package\MakemydonationImo\Controller\SinglePage\Dashboard\System;

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\Page\Controller\DashboardPageController;

class Mmdimo extends DashboardPageController
{
    public function view()
    {
        $this->redirect("/dashboard/system/mmdimo/settings");
    }
}
