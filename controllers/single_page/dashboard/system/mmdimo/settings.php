<?php
namespace Concrete\Package\MakemydonationImo\Controller\SinglePage\Dashboard\System\Mmdimo;

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Package\MakemydonationImo\Src\MakemydonationImo\API\FuneralHome;
use Concrete\Package\MakemydonationImo\Src\MakemydonationImo\AssetsLoader;
use Concrete\Core\Page\Controller\DashboardPageController;
use Config;

class Settings extends DashboardPageController
{
    public function view()
    {
        $this->set('username', Config::get('mmdimo.username'));
        $this->set('api_key', Config::get('mmdimo.api_key'));
        $this->set('fhid', Config::get('mmdimo.fhid'));
        $this->set('default_state', Config::get('mmdimo.default_state'));

        $assets = new AssetsLoader();
        $assets->loadAsset('javascript', 'mmdImoSettings');
        $assets->loadAsset('css', 'mmdImoSettings');
        $assets->registerAssets($this);
    }

    public function save_settings()
    {
        if (!$this->token->validate('save_settings')) {
            $this->error->add($this->token->getErrorMessage());
            $this->redirect('/dashboard/system/mmdimo/settings');
        }

        if (!$this->post('username')) {
            $this->error->add(t('You must provide a username.'));
        }
        if (!$this->post('api_key')) {
            $this->error->add(t('You must provide a API Key.'));
        }
        if (!$this->post('fhid')) {
            $this->error->add(t('Funeral Home not selected. Please try again and if this error persists <a href="%s">get in touch with the Make My Donation team</a>.', 'https://funerals.makemydonation.org/contact'));
        }

        if ($this->error->has()) {
            $this->view();
        }
        else {
            Config::save('mmdimo.username', $this->post('username'));
            Config::save('mmdimo.api_key', $this->post('api_key'));
            Config::save('mmdimo.fhid', $this->post('fhid'));
            Config::save('mmdimo.default_state', $this->post('default_state'));

            $this->set('success', t('Settings Saved'));
            $this->view();
        }
    }

    public function load_funeral_homes()
    {
        $funeral_home = new FuneralHome();
        $funeral_home->setAuth($this->get('username'), $this->get('api_key'));
        $funeral_home->index();

        if ($code = $funeral_home->responseCode()) {
            switch ($code) {
                case 200:
                    header('Content-type: application/json');
                    die($funeral_home->responseBody());
                default:
                    header('HTTP/1.0 404 Not Found');
                    die();
          }
        }
    }
}
