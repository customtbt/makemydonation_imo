<?php
namespace Concrete\Package\MakemydonationImo\Src\MakemydonationImo;

defined('C5_EXECUTE') or die('Access Denied.');

use Database;
use Loader;

class FHCase
{
    protected $cID;
    protected $caseID;
    protected $caseUrl;
    protected $caseData;
    protected $new = true;

    public function __construct($cID = null)
    {
        if (!is_null($cID)) {
            $db = Loader::db();
            $q = $db->Execute('select * from MmdImoCase where cID = ?', array($cID));

            if ($row = $q->fetchRow()) {
                $this->setCollectionID((int) $row['cID']);
                $this->setCaseID((int) $row['caseID']);
                $this->setCaseUrl($row['caseUrl']);
                $this->setCaseData(unserialize($row['caseData']));
                $this->new = false;
            }
            else {
                return null;
            }
        }
    }

    public function isNew()
    {
        return $this->new;
    }

    public function setCollectionID($cID)
    {
        $this->cID = $cID;
    }

    public function caseID()
    {
        return $this->caseID;
    }

    public function setCaseID($caseID)
    {
        $this->caseID = $caseID;
    }

    public function caseUrl()
    {
        return $this->caseUrl;
    }

    public function setCaseUrl($caseUrl)
    {
        $this->caseUrl = $caseUrl;
    }

    public function caseData()
    {
        return $this->caseData;
    }

    public function setCaseData($caseData)
    {
        $this->caseData = $caseData;
    }

    public function save()
    {
        $db = Loader::db();
        if ($this->new) {
            return $db->Execute('insert into MmdImoCase (cID, caseID, caseUrl, caseData) values (?, ?, ?, ?)', array($this->cID, $this->caseID, $this->caseUrl, serialize($this->caseData)));
        }
        else {
            return $db->Execute('update MmdImoCase set caseID = ?, caseUrl = ?, caseData = ? where cID = ?', array($this->caseID, $this->caseUrl, serialize($this->caseData), $this->cID));
        }
    }
}
