<?php
namespace Concrete\Package\MakemydonationImo\Src\MakemydonationImo\API;

defined('C5_EXECUTE') or die('Access Denied.');

use \Zend\Http\Client;
use \Zend\Http\Request;
use Config;

class API
{
    private $base_url = 'https://api.makemydonation.org/v1';

    protected $auth_username;

    protected $auth_api_key;

    protected $response;

    public function __construct($method = null)
    {
        $this->setAuth(Config::get('mmdimo.username'), Config::get('mmdimo.api_key'));

        if ($method !== null && is_callable(array($this, $method))) {
            call_user_func(array($this, $method));
        }
    }

    private function url()
    {
        return $this->base_url;
    }

    private function getAuth()
    {
        return array(
            'username' => $this->auth_username,
            'api_key' => $this->auth_api_key,
        );
    }

    protected function request($method, $path, $data = NULL)
    {
        $url = $this->url();
        $auth = $this->getAuth();

        $client = new Client();
        $client->setUri("$url/$path");
        $client->setAuth($auth['username'], $auth['api_key']);
        $client->setHeaders(array(
            'Content-type' => 'application/json',
        ));
        $client->setOptions(array(
            'timeout' => 30,
        ));

        switch (strtolower($method)) {
            case 'post':
                $const_method = Request::METHOD_POST;
                break;
            case 'put':
                $const_method = Request::METHOD_PUT;
                break;
            case 'get':
            default:
                $const_method = Request::METHOD_GET;
                break;
        }
        $client->setMethod($const_method);

        if ($data) {
            $json = json_encode($data);
            $client->setMethod($const_method);
            $client->setRawBody($json);
            $client->setEncType('application/json');
        }
        $this->response = $client->send();

        return $this->response;
    }

    public function setAuth($username, $api_key)
    {
        $this->auth_username = $username;
        $this->auth_api_key = $api_key;
    }

    public function responseCode()
    {
        return $this->response->getStatusCode();
    }

    public function responseBody()
    {
        return $this->response->getBody();
    }

    public function responseJson()
    {
        $json = json_decode($this->responseBody());

        if (!is_object($json)) {
            $json = (object) array();
        }

        return $json;
    }
}
