<?php
namespace Concrete\Package\MakemydonationImo\Src\MakemydonationImo\API;

defined('C5_EXECUTE') or die('Access Denied.');

class FuneralHome extends API
{
    public function index()
    {
        $this->request('get', 'funeral_home');
    }
}
