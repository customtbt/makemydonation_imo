<?php
namespace Concrete\Package\MakemydonationImo\Src\MakemydonationImo\API;

defined('C5_EXECUTE') or die('Access Denied.');

use Config;

class FuneralHomeCase extends API
{
    public function index()
    {
        $this->request('get', 'case');
    }

    public function retrieve($id)
    {
        $this->request('get', "case/$id");
    }

    public function create($data)
    {
        $data['funeral_home'] = Config::get('mmdimo.fhid');
        $this->request('post', 'case', $data);
    }

    public function update($id, $data)
    {
        $this->request('put', "case/$id", $data);
    }
}
