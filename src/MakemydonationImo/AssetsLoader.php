<?php
namespace Concrete\Package\MakemydonationImo\Src\MakemydonationImo;

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\Asset\AssetPointer;

class AssetsLoader
{
    protected $pkgHandle = 'makemydonation_imo';
    protected $assets = array();

    public function loadAsset($assetType, $assetHandle)
    {
        $this->assets[$assetType][] = $assetHandle;
    }

    public function registerAssets($controller)
    {
        $content = array();
        foreach ($this->assets as $assetType => $assetHandles) {
            foreach ($assetHandles as $assetHandle) {
                if (version_compare(APP_VERSION, '5.7.0.3', '<')) {
                    $ap = new AssetPointer($assetType, $assetHandle);
                    $a = $ap->getAsset();
                    $assetPath = str_replace(DIR_BASE_CORE, DIR_PACKAGES . '/' . $this->pkgHandle, $a->getAssetPath());

                    switch ($assetType) {
                        case 'css':
                            $content[] = '<style type="text/css">' . file_get_contents($assetPath) . '</style>';
                            break;
                        case 'javascript':
                            $content[] = '<script type="text/javascript">' . file_get_contents($assetPath) . '</script>';
                            break;
                    }
                }
                else {
                    $controller->requireAsset($assetType, $assetHandle);
                }
            }
        }

        $controller->set('assets', implode($content, "\n"));
    }
}
