<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<form method="post" action="<?php echo $view->action('save_settings'); ?>">
    <?php echo $this->controller->token->output('save_settings'); ?>

    <fieldset>
        <legend><?php echo t('In Memory Of') ?></legend>
        <div class="form-group">
            <label class="control-label" for="username"><?php echo t('Username'); ?></label>
            <input id="mmdimo-username" name="username" type="text" class="form-control" value="<?php echo $username; ?>">
            <p class="help-block">
                <?php echo t('Your Make My Donation account username.'); ?>
                <?php echo t('If you don’t have one yet,');?>
                <a href="https://funerals.makemydonation.org/user/register/affiliate" target="_blank"><?php echo t('create a free account with us');?></a>.
            </p>
        </div>

        <div class="form-group">
            <label class="control-label" for="api_key"><?php echo t('API Key'); ?></label>
            <input id="mmdimo-api-key" name="api_key" type="text" class="form-control" value="<?php echo $api_key; ?>">
            <p class="help-block">
                <?php echo t('The API Key provided by your Make My Donation account.'); ?>
                <a href="https://funerals.makemydonation.org/user/api" target="_blank"><?php echo t('Click here to get your api key'); ?></a>.
            </p>
        </div>

        <div id="mmdimo-fhid-wrapper" class="form-group">
            <label class="control-label mmdimo-fhid-label" for="fhid"><?php echo t('Funeral Home ID'); ?></label>
            <label class="control-label mmdimo-select-fhid-label" for="select_fhid" style="display: none"><?php echo t('Funeral Home'); ?></label>
            <input id="mmdimo-fhid" name="fhid" type="text" class="form-control" value="<?php echo $fhid; ?>">
            <p class="mmdimo-fhid-loading" style="display: none">
                <?php echo t('Validating your credentials...'); ?>
            </p>
            <p class="help-block mmdimo-fhid-description">
                <?php echo t('The Funeral Home ID.'); ?>
            </p>
            <p class="help-block mmdimo-select-fhid-description" style="display: none">
                <?php echo t('Select the Funeral Home you want to use.'); ?>
            </p>
        </div>

        <div class="form-group">
            <label class="control-label" for="default_state"><?php echo t('Default State'); ?></label>
            <select id="mmdimo-default-state" name="default_state" class="form-control">
              <option value="">-- <?php echo t('No default'); ?> --</option>
              <option value="AL" <?php if ('AL' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Alabama'); ?></option>
              <option value="AK" <?php if ('AK' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Alaska'); ?></option>
              <option value="AZ" <?php if ('AZ' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Arizona'); ?></option>
              <option value="AR" <?php if ('AR' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Arkansas'); ?></option>
              <option value="CA" <?php if ('CA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('California'); ?></option>
              <option value="CO" <?php if ('CO' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Colorado'); ?></option>
              <option value="CT" <?php if ('CT' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Connecticut'); ?></option>
              <option value="DE" <?php if ('DE' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Delaware'); ?></option>
              <option value="DC" <?php if ('DC' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('District of Columbia'); ?></option>
              <option value="FL" <?php if ('FL' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Florida'); ?></option>
              <option value="GA" <?php if ('GA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Georgia'); ?></option>
              <option value="HI" <?php if ('HI' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Hawaii'); ?></option>
              <option value="ID" <?php if ('ID' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Idaho'); ?></option>
              <option value="IL" <?php if ('IL' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Illinois'); ?></option>
              <option value="IN" <?php if ('IN' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Indiana'); ?></option>
              <option value="IA" <?php if ('IA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Iowa'); ?></option>
              <option value="KS" <?php if ('KS' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Kansas'); ?></option>
              <option value="KY" <?php if ('KY' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Kentucky'); ?></option>
              <option value="LA" <?php if ('LA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Louisiana'); ?></option>
              <option value="ME" <?php if ('ME' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Maine'); ?></option>
              <option value="MD" <?php if ('MD' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Maryland'); ?></option>
              <option value="MA" <?php if ('MA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Massachusetts'); ?></option>
              <option value="MI" <?php if ('MI' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Michigan'); ?></option>
              <option value="MN" <?php if ('MN' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Minnesota'); ?></option>
              <option value="MS" <?php if ('MS' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Mississippi'); ?></option>
              <option value="MO" <?php if ('MO' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Missouri'); ?></option>
              <option value="MT" <?php if ('MT' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Montana'); ?></option>
              <option value="NE" <?php if ('NE' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Nebraska'); ?></option>
              <option value="NV" <?php if ('NV' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Nevada'); ?></option>
              <option value="NH" <?php if ('NH' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('New Hampshire'); ?></option>
              <option value="NJ" <?php if ('NJ' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('New Jersey'); ?></option>
              <option value="NM" <?php if ('NM' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('New Mexico'); ?></option>
              <option value="NY" <?php if ('NY' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('New York'); ?></option>
              <option value="NC" <?php if ('NC' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('North Carolina'); ?></option>
              <option value="ND" <?php if ('ND' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('North Dakota'); ?></option>
              <option value="OH" <?php if ('OH' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Ohio'); ?></option>
              <option value="OK" <?php if ('OK' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Oklahoma'); ?></option>
              <option value="OR" <?php if ('OR' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Oregon'); ?></option>
              <option value="PA" <?php if ('PA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Pennsylvania'); ?></option>
              <option value="RI" <?php if ('RI' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Rhode Island'); ?></option>
              <option value="SC" <?php if ('SC' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('South Carolina'); ?></option>
              <option value="SD" <?php if ('SD' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('South Dakota'); ?></option>
              <option value="TN" <?php if ('TN' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Tennessee'); ?></option>
              <option value="TX" <?php if ('TX' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Texas'); ?></option>
              <option value="UT" <?php if ('UT' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Utah'); ?></option>
              <option value="VT" <?php if ('VT' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Vermont'); ?></option>
              <option value="VA" <?php if ('VA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Virginia'); ?></option>
              <option value="WA" <?php if ('WA' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Washington'); ?></option>
              <option value="WV" <?php if ('WV' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('West Virginia'); ?></option>
              <option value="WI" <?php if ('WI' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Wisconsin'); ?></option>
              <option value="WY" <?php if ('WY' == $default_state) { echo 'selected="selected"'; } ?>><?php echo t('Wyoming'); ?></option>
            </select>
            <p class="help-block">
                <?php echo t('The default state to search for charities when creating a new post.'); ?>
            </p>
        </div>
    </fieldset>

    <div class="ccm-dashboard-form-actions-wrapper">
        <div class="ccm-dashboard-form-actions">
            <?php echo $interface->submit(t('Save'), 'mmdimo-form', 'right', 'btn-primary'); ?>
        </div>
    </div>
</form>

<script type="text/javascript">
var loadFuneralHomesAction = '<?php echo str_replace('&amp;', '&', $view->action('load_funeral_homes')); ?>';
var loadFuneralHomesStatusSuccessOne = '<?php echo t('Credentials validated. Using the default funeral home'); ?>';
var loadFuneralHomesStatusSuccessMultiple = '<?php echo t('Credentials validated. Please choose the desired funeral home.'); ?>';
var loadFuneralHomesStatusInvalidAuth = '<?php echo t('Invalid username or API Key.'); ?>';
var loadFuneralHomesStatusFHNotFound = '<?php echo t('Could not find any funeral homes with your account. Please <a href="%s">get in touch with the Make My Donation team</a>.', 'https://funerals.makemydonation.org/contact'); ?>';
</script>

<?php print $assets; ?>
