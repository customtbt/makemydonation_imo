jQuery(function($) {

$('form').each(function() {
    var $form = $(this);
    var $username = $('#mmdimo-username', this);
    var $usernameWrapper = $username.closest('.form-group');
    var $apikey = $('#mmdimo-api-key', this);
    var $apikeyWrapper = $apikey.closest('.form-group');
    var $fhid = $('#mmdimo-fhid', this);
    var $fhidWrapper = $('#mmdimo-fhid-wrapper', this);
    var $fhidLoading = $fhidWrapper.find('.mmdimo-fhid-loading');
    var $fhidLabel = $fhidWrapper.find('.mmdimo-fhid-label');
    var $fhidDescription = $fhidWrapper.find('.mmdimo-fhid-description');
    var $selectFHID = $fhid.after('<select id="mmdimo-select-fhid"></select>').next('#mmdimo-select-fhid');
    var $selectFHIDLabel = $fhidWrapper.find('.mmdimo-select-fhid-label');
    var $selectFHIDDescription = $fhidWrapper.find('.mmdimo-select-fhid-description');
    var $fhidStatus = $selectFHID.closest('#mmdimo-fhid-wrapper').before('<div class="mmdimo-status"><p class="help-block"></p></div>').prev('.mmdimo-status').hide();

    $fhidWrapper.find('label').text('Funeral Home');

    $selectFHID.hide();

    var mmdimoAuthFieldChange = function() {
        var user = $username.val();
        var key = $apikey.val();

        if (user && key) {
            $usernameWrapper.removeClass('has-success has-error');
            $apikeyWrapper.removeClass('has-success has-error');

            $fhid.hide();
            $fhidLabel.hide();
            $fhidDescription.hide();
            $selectFHIDLabel.hide();
            $selectFHIDDescription.hide();
            $fhidStatus
                .removeClass('has-success has-error')
                .hide();

            $fhidWrapper.show();
            $fhidLoading.show();

            $.ajax(loadFuneralHomesAction, {
                dataType: 'json',
                data: {
                    username: user,
                    api_key: key,
                },
                complete: function() {
                    $fhidLoading.fadeOut('slow');
                    if ($selectFHID.find('option').length <= 1) {
                        $fhidWrapper.fadeOut('slow');
                    }
                },
                success: function(data, status, xhr) {
                    var funeralHomes = xhr.responseJSON;
                    var defaultFuneralHome;

                    $selectFHID.html('');
                    if (funeralHomes.length) {
                        $.each(funeralHomes, function(k, funeralHome) {
                            defaultFuneralHome = funeralHome;
                            var option = '<option value="' + funeralHome.id + '" ' + ($fhid.val() == funeralHome.id ? 'selected="selected"' : '') + '>' + funeralHome.name + '</option>';
                            $selectFHID.append(option);
                        });
                        $selectFHID.trigger('change');

                        $usernameWrapper.addClass('has-success');
                        $apikeyWrapper.addClass('has-success');

                        $fhidStatus
                            .addClass('has-success')
                            .show()
                            .find('p').html(loadFuneralHomesStatusSuccessOne + ' ' + defaultFuneralHome.name + '.');

                        if (funeralHomes.length > 1) {
                            $fhidWrapper.show();
                            $selectFHIDLabel.show();
                            $selectFHIDDescription.show();
                            $selectFHID.show();

                            $fhidStatus
                                .addClass('has-success')
                                .show()
                                .find('p').html(loadFuneralHomesStatusSuccessMultiple);
                        }
                    }
                    else {
                        $usernameWrapper.addClass('has-error');
                        $apikeyWrapper.addClass('has-error');

                        $fhidStatus
                            .addClass('has-error')
                            .show()
                            .find('p').html(loadFuneralHomesStatusFHNotFound);
                    }
                },
                statusCode: {
                    404: function() {
                            $usernameWrapper.addClass('has-error');
                            $apikeyWrapper.addClass('has-error');
                    },
                }
            });
        }
    };

    $username.bind('change', mmdimoAuthFieldChange);
    $apikey.bind('change', mmdimoAuthFieldChange);

    $selectFHID.bind('change', function() {
        $fhid.val($(this).val());
    });

    mmdimoAuthFieldChange();
});

});
