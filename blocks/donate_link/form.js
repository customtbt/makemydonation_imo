var makeMyDonationImoBlockForm;

jQuery(function($) {

makeMyDonationImoBlockForm = function() {
    $('form:not(.make-my-donation-imo-block-form)').each(function() {
        var $form = $(this);
        var $dialog = $('.ui-dialog-content');
        var $state = $('#mmdimo-charity-state', $form);
        var $charity = $('#mmdimo-charity', $form);
        var $select = $('#mmdimo-charity-select input', $form);
        var $all = $('#mmdimo-charity-select-all', $form);
        var $single = $('#mmdimo-charity-select-single', $form);
        var $charitySelect = $('#mmdimo-charity-single-select', $form);
        var $charityGroup = $('.mmdimo-field-charity.form-group', $form);
        var $charityMeta = $('#mmdimo-charity-metadata', $form);
        var $charityName = $charity.after('<input type="text" id="mmdimo-charity-name" class="form-control" size="40">').next('#mmdimo-charity-name');
        var $charityActive = $charitySelect.append('<a id="mmdimo-charity-active" title="Select another charity"></a>').find('#mmdimo-charity-active').hide();
        var options, engine, dataset;

        if ($charity.length && $charityName.length) {
            $charity.hide().addClass('hidden');

            $charityName
                .attr('autocomplete', 'OFF')
                .attr('aria-autocomplete', 'list');

            $state.bind('change', function() {
                var st = $(this).val();
                var state = '';

                if (st) {
                    state = $state.find('option[value="' + $(this).val() + '"]').text() + ' ';
                }

                $charityName.attr('placeholder', 'Search all ' + state + 'charities');
            }).trigger('change');

            engine = new Bloodhound({
                identify: function(o) { return o.id_str; },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace(),
                dupDetector: function(a, b) { return a.id_str === b.id_str; },
                remote: {
                    url: 'https://funerals.makemydonation.org/orghunter/charitysearch/autocomplete/%QUERY/typeahead?eligible=1',
                    prepare: function(query, settings) {
                        var state = $state.val();
                        settings.url = settings.url.replace('%QUERY', encodeURIComponent(query));
                        if (state) {
                            settings.url = settings.url + '&state=' + state;
                        }
                        return settings;
                    },
                    transform: function(results) {
                        var arr = [];

                        for (var key in results) {
                            if (results.hasOwnProperty(key)) {
                                arr.push(results[key]);
                            }
                        }

                        arr.sort(function(a, b) {
                            if (a.weight && b.weight) {
                                if (a.weight < b.weight) {
                                    return -1;
                                }
                                if (a.weight > b.weight) {
                                    return 1;
                                }
                            }
                            return 0;
                        });

                        return arr;
                    },
                },
            });
            options = {
                classNames: {
                    input: 'form-control',
                }
            };
            dataset = {
                source: engine,
                display: function(suggestion) {
                    return suggestion.name;
                },
                templates: {
                    suggestion: function(suggestion) {
                        return '<div><strong>' + suggestion.name + '</strong><br><span>EIN: ' + suggestion.ein + ' — ' + suggestion.city + ', ' + suggestion.state + '</span></div>';
                    }
                },
            };

            $charityName
                .typeahead(options, dataset)
                .bind('typeahead:select', function(event, suggestion) {
                    var metadata = {
                        state: $state.val(),
                        charity: suggestion,
                    };

                    $charityActive
                        .html('<strong>' + suggestion.name + '</strong><br><span>EIN: ' + suggestion.ein + ' — ' + suggestion.city + ', ' + suggestion.state + '</span>')
                        .show()
                        .removeClass('hidden');
                    $charity.val(suggestion.ein);

                    $charityMeta.val(JSON.stringify(metadata));

                    $('.twitter-typeahead', $form).hide().addClass('hidden');
                    $state.hide().addClass('hidden');
                    $charityName.hide().addClass('hidden');
                });

            $dialog.addClass('typeahead');

            if ($charityMeta.val()) {
                var metadata = $.parseJSON($charityMeta.val());
                var suggestion = metadata.charity;

                $charityActive
                    .html('<strong>' + suggestion.name + '</strong><br><span>EIN: ' + suggestion.ein + ' — ' + suggestion.city + ', ' + suggestion.state + '</span>')
                    .show()
                    .removeClass('hidden');

                $state.val(metadata.state);
                $charityName.val(suggestion.name);

                $('.twitter-typeahead', $form).hide().addClass('hidden');
                $state.hide().addClass('hidden');
                $charityName.hide().addClass('hidden');
            }

            $charityActive.bind('click', function(event) {
                $charityActive.hide().addClass('hidden');
                $('.twitter-typeahead', $form).show().removeClass('hidden');
                $state.show().removeClass('hidden');
                $charityName.show().removeClass('hidden').focus();

                event.stopPropagation();
                return false;
            });

            $select
                .bind('change', function() {
                    if ($single.is(':checked')) {
                        $charitySelect.show();
                    }
                    else {
                        $charitySelect.hide();
                    }
                })
                .trigger('change');
        }

        $form.addClass('make-my-donation-imo-block-form');
    });
};

makeMyDonationImoBlockForm();

});
