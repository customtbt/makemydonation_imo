<?php
namespace Concrete\Package\MakemydonationImo\Block\DonateLink;

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Package\MakemydonationImo\Src\MakemydonationImo\API\FuneralHomeCase;
use Concrete\Package\MakemydonationImo\Src\MakemydonationImo\AssetsLoader;
use Concrete\Package\MakemydonationImo\Src\MakemydonationImo\FHCase;
use Concrete\Core\Block\BlockController;
use Config;
use Loader;
use Page;

class Controller extends BlockController
{
    protected $btTable = 'btMmdImoDonateLink';
    protected $btCacheBlockOutput = true;
    protected $btInterfaceHeight = 440;

    protected $linkContentDefault = null;
    protected $linkTitleDefault = null;

    public function getBlockTypeName()
    {
        return t('Donate Link');
    }

    public function getBlockTypeDescription()
    {
        return t('Link to donate for the current obituary on Make My Donation - In Memory Of.');
    }

    public function add()
    {
        $this->setDefaultProperties();
        $this->setDefaultAttributes();
        $this->setCaseAttributes();
        $this->addFormAssets();
    }

    public function edit()
    {
        $this->setDefaultProperties();
        $this->setDefaultAttributes();
        $this->setCaseAttributes();
        $this->addFormAssets();
    }

    public function setDefaultProperties()
    {
        if (is_null($this->linkContentDefault) || is_null($this->linkTitleDefault)) {
            $page = Page::getCurrentPage();

            $this->linkContentDefault = t('Donate');
            $this->linkTitleDefault = t('Make My Donation in Memory of %s', $page->getCollectionName());
        }
    }

    public function setDefaultAttributes()
    {
        $this->set('link_content_default', $this->linkContentDefault);
        $this->set('link_title_default', $this->linkTitleDefault);
        $this->set('state', Config::get('mmdimo.default_state'));
    }

    public function setCaseAttributes()
    {
        $c = Page::getCurrentPage();

        $case = new FHCase($c->cID);

        if ($caseData = $case->caseData()) {
            if (is_array($caseData)) {
                if (isset($caseData['family_email'])) {
                    $this->set('family_email', $caseData['family_email']);
                }
                if (isset($caseData['charity_metadata'])) {
                    $this->set('charity_metadata', $caseData['charity_metadata']);

                    $charityData = json_decode($caseData['charity_metadata']);

                    if (isset($charityData->charity) && isset($charityData->charity->ein)) {
                        $this->set('charity', $charityData->charity->ein);
                    }
                    if (isset($charityData->state)) {
                        $this->set('state', $charityData->state);
                    }
                }
            }
        }
    }

    public function setCaseVariables()
    {
        $c = Page::getCurrentPage();

        $case = new FHCase($c->cID);

        if ($url = $case->caseUrl()) {
            $nav = Loader::helper('navigation');
            $rurl = $url . '?r=' . urlencode($nav->getCollectionURL($c));
            $this->set('linkUrl', $rurl);
        }
    }

    public function addFormAssets()
    {
        $assets = new AssetsLoader();

        $assets->loadAsset('javascript', 'mmdImoTypeahead');
        $assets->loadAsset('javascript', 'mmdImoForm');
        $assets->loadAsset('css', 'mmdImoForm');

        $assets->registerAssets($this);
    }

    public function view()
    {
        $this->setDefaultProperties();
        $this->setCaseAttributes();
        $this->setCaseVariables();

        if (!$this->get('linkContent')) {
            $this->set('linkContent', $this->linkContentDefault);
        }
        if (!$this->get('linkTitle')) {
            $this->set('linkTitle', $this->linkTitleDefault);
        }
    }

    public function validate($data)
    {
        $e = Loader::helper('validation/error');

        if ($data['charity'] && !is_numeric($data['charity']) && strlen($data['charity']) !== 9) {
            $e->add(t('Charity value is invalid.'));
        }

        if ($data['family_email'] && !filter_var($data['family_email'], FILTER_VALIDATE_EMAIL)) {
            $e->add(t('Family email is invalid.'));
        }

        return $e;
    }

    public function save($data)
    {
        $c = Page::getCurrentPage();

        $case = new FHCase($c->cID);

        if ($case->isNew()) {
            $rcb = array(
                'name' => $c->getCollectionName(),
                'charity' => $data['charity'],
                'family_email' => $data['family_email'],
                'family_notify' => (string) (int) $data['family_notify'],
            );
            $fhc = new FuneralHomeCase();
            $fhc->create($rcb);

            $remoteCaseData = $fhc->responseJson();

            $caseData = array(
                'family_email' => $data['family_email'],
                'charity_metadata' => $data['charity_metadata'],
            );

            $case->setCollectionID($c->cID);
            $case->setCaseID($remoteCaseData->id);
            $case->setCaseUrl($remoteCaseData->url);
            $case->setCaseData($caseData);
            $case->save();
        }
        else {
          $fhc = new FuneralHomeCase();
          $fhc->retrieve($case->caseID());

          $remoteCaseData = $fhc->responseJson();
          $remoteCaseData->name = $c->getCollectionName();
          $remoteCaseData->charity = $data['charity'];
          $remoteCaseData->family_email = $data['family_email'];
          $remoteCaseData->family_notify = (string) (int) $data['family_notify'];

          $ufhc = new FuneralHomeCase();
          $ufhc->update($remoteCaseData->id, $remoteCaseData);
          $uremoteCaseData = $ufhc->responseJson();

          $caseData = array(
              'family_email' => $data['family_email'],
              'charity_metadata' => $data['charity_metadata'],
          );
          $case->setCaseData($caseData);
          $case->setCaseUrl($uremoteCaseData->url);
          $case->save();
        }

        parent::save($data);
    }
}
