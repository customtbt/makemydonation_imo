<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div class='form-horizontal'>
    <div class="form-group mmdimo-link-content">
        <label for="mmdimo-link-content" class="col-sm-3 control-label"><?php echo t('Link Text'); ?></label>
        <div class="col-sm-9">
            <input type="text" id="mmdimo-link-content" name="linkContent" value="<?php echo $linkContent; ?>" placeholder="<?php echo $link_content_default; ?>" class="form-control">
        </div>
    </div>

    <div class="form-group mmdimo-link-title">
        <label for="mmdimo-link-title" class="col-sm-3 control-label"><?php echo t('Hover Text'); ?></label>
        <div class="col-sm-9">
            <input type="text" id="mmdimo-link-title" name="linkTitle" value="<?php echo $linkTitle; ?>" placeholder="<?php echo $link_title_default; ?>" class="form-control">
        </div>
    </div>

    <div class="checkbox mmdimo-link-target-blank">
        <label>
            <input value="1" type="checkbox" name="linkTargetBlank" id="mmdimo-link-target-blank" <?php if ($linkTargetBlank) { echo 'checked="checked"'; } ?>>
            <?php echo t('Open link in a new page'); ?>
        </label>
    </div>
</div>

<hr>

<div class="form-group mmdimo-field-charity">
    <label for="mmdimo-charity" class="control-label"><?php echo t('Charity'); ?></label>
    <div id="mmdimo-charity-select">
        <div class="radio">
            <label class="mmdimo-charity-select-all control-label">
                <input type="radio" name="mmdimo_charity_select" id="mmdimo-charity-select-all" value="all" <?php if (!$charity): ?>checked="checked"<?php endif; ?>>
                <?php echo t('Allow donations to any charity'); ?>
            </label>
        </div>
        <div class="radio">
            <label class="mmdimo-charity-select-single control-label">
                <input type="radio" name="mmdimo_charity_select" id="mmdimo-charity-select-single" value="single" <?php if ($charity): ?>checked="checked"<?php endif; ?>>
                <?php echo t('Select a single charity'); ?>
            </label>
        </div>
    </div>
    <div id="mmdimo-charity-single-select" class="row">
        <div class="col-md-4">
            <select id="mmdimo-charity-state" name="mmdimocharity_state" class="form-control">
                <option value=""><?php echo t('All states', 'mmdimo'); ?></option>
                <option value="AL" <?php if ('AL' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Alabama'); ?></option>
                <option value="AK" <?php if ('AK' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Alaska'); ?></option>
                <option value="AZ" <?php if ('AZ' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Arizona'); ?></option>
                <option value="AR" <?php if ('AR' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Arkansas'); ?></option>
                <option value="CA" <?php if ('CA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('California'); ?></option>
                <option value="CO" <?php if ('CO' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Colorado'); ?></option>
                <option value="CT" <?php if ('CT' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Connecticut'); ?></option>
                <option value="DE" <?php if ('DE' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Delaware'); ?></option>
                <option value="DC" <?php if ('DC' == $state) { echo 'selected="selected"'; } ?>><?php echo t('District of Columbia'); ?></option>
                <option value="FL" <?php if ('FL' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Florida'); ?></option>
                <option value="GA" <?php if ('GA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Georgia'); ?></option>
                <option value="HI" <?php if ('HI' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Hawaii'); ?></option>
                <option value="ID" <?php if ('ID' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Idaho'); ?></option>
                <option value="IL" <?php if ('IL' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Illinois'); ?></option>
                <option value="IN" <?php if ('IN' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Indiana'); ?></option>
                <option value="IA" <?php if ('IA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Iowa'); ?></option>
                <option value="KS" <?php if ('KS' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Kansas'); ?></option>
                <option value="KY" <?php if ('KY' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Kentucky'); ?></option>
                <option value="LA" <?php if ('LA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Louisiana'); ?></option>
                <option value="ME" <?php if ('ME' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Maine'); ?></option>
                <option value="MD" <?php if ('MD' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Maryland'); ?></option>
                <option value="MA" <?php if ('MA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Massachusetts'); ?></option>
                <option value="MI" <?php if ('MI' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Michigan'); ?></option>
                <option value="MN" <?php if ('MN' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Minnesota'); ?></option>
                <option value="MS" <?php if ('MS' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Mississippi'); ?></option>
                <option value="MO" <?php if ('MO' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Missouri'); ?></option>
                <option value="MT" <?php if ('MT' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Montana'); ?></option>
                <option value="NE" <?php if ('NE' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Nebraska'); ?></option>
                <option value="NV" <?php if ('NV' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Nevada'); ?></option>
                <option value="NH" <?php if ('NH' == $state) { echo 'selected="selected"'; } ?>><?php echo t('New Hampshire'); ?></option>
                <option value="NJ" <?php if ('NJ' == $state) { echo 'selected="selected"'; } ?>><?php echo t('New Jersey'); ?></option>
                <option value="NM" <?php if ('NM' == $state) { echo 'selected="selected"'; } ?>><?php echo t('New Mexico'); ?></option>
                <option value="NY" <?php if ('NY' == $state) { echo 'selected="selected"'; } ?>><?php echo t('New York'); ?></option>
                <option value="NC" <?php if ('NC' == $state) { echo 'selected="selected"'; } ?>><?php echo t('North Carolina'); ?></option>
                <option value="ND" <?php if ('ND' == $state) { echo 'selected="selected"'; } ?>><?php echo t('North Dakota'); ?></option>
                <option value="OH" <?php if ('OH' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Ohio'); ?></option>
                <option value="OK" <?php if ('OK' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Oklahoma'); ?></option>
                <option value="OR" <?php if ('OR' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Oregon'); ?></option>
                <option value="PA" <?php if ('PA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Pennsylvania'); ?></option>
                <option value="RI" <?php if ('RI' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Rhode Island'); ?></option>
                <option value="SC" <?php if ('SC' == $state) { echo 'selected="selected"'; } ?>><?php echo t('South Carolina'); ?></option>
                <option value="SD" <?php if ('SD' == $state) { echo 'selected="selected"'; } ?>><?php echo t('South Dakota'); ?></option>
                <option value="TN" <?php if ('TN' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Tennessee'); ?></option>
                <option value="TX" <?php if ('TX' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Texas'); ?></option>
                <option value="UT" <?php if ('UT' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Utah'); ?></option>
                <option value="VT" <?php if ('VT' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Vermont'); ?></option>
                <option value="VA" <?php if ('VA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Virginia'); ?></option>
                <option value="WA" <?php if ('WA' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Washington'); ?></option>
                <option value="WV" <?php if ('WV' == $state) { echo 'selected="selected"'; } ?>><?php echo t('West Virginia'); ?></option>
                <option value="WI" <?php if ('WI' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Wisconsin'); ?></option>
                <option value="WY" <?php if ('WY' == $state) { echo 'selected="selected"'; } ?>><?php echo t('Wyoming'); ?></option>
            </select>
        </div>

        <div class="col-md-8">
            <input type="text" id="mmdimo-charity" name="charity" value="<?php echo $charity; ?>" class="form-control">
        </div>
    </div>

    <input type="hidden" id="mmdimo-charity-metadata" name="charity_metadata" value='<?php echo $charity_metadata; ?>'>
</div>

<div class="form-group mmdimo-field-family-email">
    <label for="mmdimo-family-email" class="control-label"><?php echo t('Family email'); ?></label>

    <div class="input-group">
        <input type="text" id="mmdimo-family-email" name="family_email" value="<?php echo $family_email; ?>" class="form-control">
        <span class="input-group-addon">
            <label title="<?php echo t('Send an email to the family to let them know about the new created donation page.'); ?>">
                <input value="1" type="checkbox" name="family_notify" id="mmdimo-family-notify">
                <?php echo t('Notify the family'); ?>
            </label>
        </span>
    </div>
</div>

<?php print $assets; ?>
<script type="text/javascript">
if (typeof makeMyDonationImoBlockForm == 'function') {
  makeMyDonationImoBlockForm();
}
</script>
