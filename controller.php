<?php
namespace Concrete\Package\MakemydonationImo;

defined('C5_EXECUTE') or die('Access Denied.');

use Concrete\Core\Block\BlockType\BlockType;
use Concrete\Core\Foundation\ModifiedPSR4ClassLoader as SymfonyClassloader;
use Concrete\Core\Package\Package;
use Concrete\Core\Page\Single as SinglePage;
use Asset;
use AssetList;
use Config;

class Controller extends Package
{
    protected $pkgHandle = 'makemydonation_imo';
    protected $appVersionRequired = '5.7.0.4';
    protected $pkgVersion = '1.0.5';

    protected $classHandle = 'MakemydonationImo';

    public function getPackageName()
    {
        return t('Make My Donation - In Memory Of');
    }

    public function getPackageDescription()
    {
        return t('Integrate your funeral home site with our Make My Donation - In Memory Of Platform and allow donations to over 1.4 million eligible US charities.');
    }

    public function install()
    {
        $pkg = parent::install();

        BlockType::installBlockType('donate_link', $pkg);

        $parent_page = SinglePage::add('/dashboard/system/mmdimo', $pkg);
        $parent_page->updateCollectionName(t('Make My Donation'));

        $page = SinglePage::add('/dashboard/system/mmdimo/settings', $pkg);
        $page->updateCollectionName(t('Settings'));
    }

    public function uninstall()
    {
        Config::clear('mmdimo.username');
        Config::clear('mmdimo.api_key');
        Config::clear('mmdimo.fhid');
        Config::clear('mmdimo.default_state');

        parent::uninstall();
    }

    public function on_start()
    {
        $pkgHandle = $this->pkgHandle;
        $classHandle = $this->classHandle;

        $assets = array(
            'mmdImoSettings' => array(
                'css' => array(
                    'css',
                    'single_pages/dashboard/system/mmdimo/settings.css',
                    array(),
                    $this,
                ),
                'javascript' => array(
                    'javascript',
                    'single_pages/dashboard/system/mmdimo/settings.js',
                    array(),
                    $this,
                ),
            ),
            'mmdImoForm' => array(
                'css' => array(
                    'css',
                    'blocks/donate_link/form.css',
                    array(),
                    $this,
                ),
                'javascript' => array(
                    'javascript',
                    'blocks/donate_link/form.js',
                    array(),
                    $this,
                ),
            ),
            'mmdImoTypeahead' => array(
                'javascript' => array(
                    'javascript',
                    'lib/typeahead/typeahead.bundle.min.js',
                    array(
                        'version' => '0.11.1',
                    ),
                    $this,
                ),
            ),
        );

        $al = AssetList::getInstance();
        $al->registerMultiple($assets);

        $loader = new SymfonyClassloader();
        $loader->addPrefix(NAMESPACE_SEGMENT_VENDOR . '\\Package\\' . $classHandle, DIR_PACKAGES . '/' . $pkgHandle);
        $loader->addPrefix(NAMESPACE_SEGMENT_VENDOR . '\\Package\\' . $classHandle . '\\Src', DIR_PACKAGES . '/' . $pkgHandle . '/' . DIRNAME_CLASSES);
        $loader->addPrefix(NAMESPACE_SEGMENT_VENDOR . '\\Package\\' . $classHandle . '\\Src\\' . $classHandle, DIR_PACKAGES . '/' . $pkgHandle . '/' . DIRNAME_CLASSES . '/' . $classHandle);
        $loader->register();
    }

}
